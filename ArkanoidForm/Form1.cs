﻿/***********************************************************************************
 *                                                                                  * 
 * Autore:Denny Schwender                                                           *
 * Simulazione di Arkanoid e Breakout                                               *
 * Progetto di semestre del modulo 318 nel linguaggio C#                            *  
 *                                                                                  *
 ************************************************************************************/

using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace ArkanoidForm
{
    public partial class ArkanoidForm : Form
    {
        /*
         * Istanzio tutte le variabili e gli oggetti che usero nel mio progetto
         */
        Rectangle r, palla;
        Rectangle[][] level1;
        bool[][] game;
        Pen pen;
        SolidBrush drawBrush;
        Font drawFont;
        bool isMenu;
        bool finishGame;
        bool win;
        int velocitaX;
        int velocitaY;
        int rectHeight, rectWidth, rectX, rectY, pallaX, pallaY, punteggio;
        float StringX, StringY;


        public ArkanoidForm()
        {
            InitializeComponent();
        }

        /*
         * Usato per mettere i dati ai valori di default
         */
        private void RestoreData()
        {
            level1 = new Rectangle[3][];
            game = new bool[3][];
            pen = new Pen(Color.Black);
            drawBrush = new SolidBrush(Color.Black);
            drawFont = new Font("Arial", 16);
            isMenu = true;
            finishGame = false;
            win = false;
            velocitaX = -5;
            velocitaY = -5;
            pallaX = 50;
            pallaY = ClientRectangle.Height / 10 * 7;
            rectHeight = 20;
            rectWidth = 150;
            StringX = 150.0F;
            StringY = 150.0F;
            punteggio = 0;
            level1[0] = new Rectangle[10];
            level1[1] = new Rectangle[10];
            level1[2] = new Rectangle[10];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    level1[i][j] = new Rectangle(j * ClientRectangle.Width / 10, i * ClientRectangle.Height / 3 / 3, ClientRectangle.Width / 10, ClientRectangle.Height / 3 / 3);
                }
            }
        }

        /*
        * Ho creato questo metodo che attraverso il Threading con una frequenza di 30 frame al secondo
        * invalida la Form e la fa ridisegnare
        */
        public void RefreshWindow()
        {
            while (true)
            {
                Thread.Sleep(1000 / 30);
                this.Invalidate();
            }

        }

        /*
         * La paint che contiene tutta la logica del gioco
         * Viene richiamata ogni volta che il Thread invalida la Form
         */
        private void ArkanoidForm_Paint(object sender, PaintEventArgs e)
        {
            int morti = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (level1[i][j] == Rectangle.Empty)
                    {
                        morti++;
                    }
                }
            }
            if (morti == 30)
            {
                isMenu = true;
                finishGame = true;
                win = true;
                Cursor.Show();
            }


            if (isMenu)
            {
                if (finishGame)
                {
                    e.Graphics.DrawString("Gioco sviluppato da Denny Schwender\nClasse I3AA\nProgetto semestrale di C#\nMod 318", drawFont, drawBrush, this.Bounds.Right - 500, this.Bounds.Bottom - 500);
                    if (win)
                    {
                        e.Graphics.DrawString("Hai Vinto", drawFont, drawBrush, StringX, StringY);
                        e.Graphics.DrawString("Il tuo Punteggio: " + punteggio, drawFont, drawBrush, StringX, StringY + 50);
                        e.Graphics.DrawString("Rigioca", drawFont, drawBrush, StringX, StringY + 100);
                        e.Graphics.DrawString("Chiudi", drawFont, drawBrush, StringX, StringY + 150);
                    }
                    else
                    {
                        e.Graphics.DrawString("Hai perso", drawFont, drawBrush, StringX, StringY);
                        //e.Graphics.DrawString("Il tuo Punteggio: " + punteggio, drawFont, drawBrush, StringX, StringY + 50);
                        e.Graphics.DrawString("Rigioca", drawFont, drawBrush, StringX, StringY + 50);
                        e.Graphics.DrawString("Chiudi", drawFont, drawBrush, StringX, StringY + 100);

                    }
                }
                else
                {
                    e.Graphics.DrawString("Gioca", drawFont, drawBrush, StringX, StringY);
                    e.Graphics.DrawString("Gioco sviluppato da Denny Schwender\nClasse I3AA\nProgetto semestrale di C#\nMod 318", drawFont, drawBrush, StringX, StringY + 100);
                }
            }
            else
            {
                palla = new Rectangle(pallaX, pallaY, 20, 20);
                pallaX += velocitaX;
                pallaY += velocitaY;
                e.Graphics.DrawRectangle(pen, r);
                e.Graphics.DrawEllipse(pen, palla);
                e.Graphics.DrawString(punteggio + "", drawFont, drawBrush, this.Bounds.Right - 50, this.Bounds.Bottom - 50);
                for (int u = 0; u < level1.Length; u++)
                {
                    e.Graphics.DrawRectangles(pen, level1[u]);
                }

                if (pallaX <= this.Bounds.Left + 5 || pallaX >= this.Bounds.Right - 5)
                {
                    velocitaX *= -1;
                }
                if (pallaY <= this.Bounds.Top + 5)
                {
                    velocitaY *= -1;
                }
                if (pallaX + 20 <= r.Right && pallaX + 20 >= r.Left && pallaY + 20 <= r.Top + 5 && pallaY + 20 >= r.Top - 5)
                {
                    int latoR = r.Right - r.Left;
                    if ((pallaX + 20 < r.Left + latoR / 4) || pallaX + 20 > r.Right - latoR / 4)
                    {
                        velocitaY *= -1;
                        velocitaX *= -1;
                    }
                    else
                    {
                        velocitaY *= -1;
                    }
                    punteggio += 100;
                    if (velocitaX <= 0)
                    {
                        velocitaX -= 2;
                    }
                    else
                    {
                        velocitaX += 2;
                    }
                    if (velocitaY <= 0)
                    {
                        velocitaY -= 2;
                    }
                    else
                    {
                        velocitaY += 2;
                    }
                }
                if (pallaY >= this.Bounds.Bottom - 5)
                {
                    isMenu = true;
                    finishGame = true;
                    win = false;
                    Cursor.Show();
                }

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if (palla.IntersectsWith(level1[i][j]))
                        {
                            if ((pallaY <= level1[i][j].Bottom && pallaY + 20 >= level1[i][j].Bottom) || (pallaY <= level1[i][j].Top && pallaY + 20 >= level1[i][j].Top))
                            {
                                velocitaY *= -1;
                            }
                            else
                            {
                                velocitaX *= -1;
                            }
                            punteggio += 150;
                            level1[i][j] = Rectangle.Empty;
                        }
                    }
                }


            }
        }

        /*
         * Questo metodo viene chiamato al caricamento dell'applicazione
         * Contiene lo start del Thread secondario sul metodo presentato precedentemente
         */
        private void ArkanoidForm_Load(object sender, EventArgs e)
        {
            Thread newThread = new Thread(RefreshWindow);
            newThread.Start();
            RestoreData();

        }

        /*
         * Questo evento gestisce la barra che l'utente muove con il suo mouse
         */
        private void ArkanoidForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X < rectWidth / 2)
            {
                rectX = 0;
            }
            else if (e.X > ClientRectangle.Width - rectWidth / 2)
            {
                rectX = ClientRectangle.Width - rectWidth;
            }
            else
            {
                rectX = e.X - rectWidth / 2;
            }
            rectY = ClientRectangle.Height / 10 * 8;
            r = new Rectangle(rectX, rectY, rectWidth, rectHeight);
        }

        /*
         * Evento che gestisce i click sulle stringhe all'inizio e alla fine del gioco
         */
        private void ArkanoidForm_MouseClick(object sender, MouseEventArgs e)
        {
            if (isMenu)
            {
                if (finishGame)
                {
                    if (win)
                    {
                        if (e.X >= StringX && e.X <= StringX + 500 && e.Y >= StringY + 150 && e.Y <= StringY + 190)
                        {
                            this.Close();
                        }
                        if (e.X >= StringX && e.X <= StringX + 500 && e.Y >= StringY + 100 && e.Y <= StringY + 140)
                        {
                            RestoreData();
                            this.Invalidate();
                        }
                    }
                    else
                    {
                        if (e.X >= StringX && e.X <= StringX + 500 && e.Y >= StringY + 100 && e.Y <= StringY + 140)
                        {
                            this.Close();
                        }
                        if (e.X >= StringX && e.X <= StringX + 500 && e.Y >= StringY + 50 && e.Y <= StringY + 90)
                        {
                            RestoreData();
                            this.Invalidate();
                        }
                    }
                }
                else
                {
                    if (e.X > StringX && e.Y > StringY && e.X < 200F && e.Y < 200F)
                    {
                        isMenu = false;
                        Cursor.Hide();
                    }
                }
            }
        }
    }
}
